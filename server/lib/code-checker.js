'use strict';


const Joi = require('joi');


const SLOT_OBJECT_SCHEMA = Joi.object({
	start: Joi.object({
		hour: Joi.number().min(0).max(24).required(),
		minute: Joi.number().min(0).max(59).required(),
		second: Joi.number().min(0).max(59).required(),
		weekday: Joi.number().min(0).max(6).required(),
		day: Joi.alternatives()
			.try(Joi.number().min(1).max(31).required(),
				Joi.string().valid('*')),
		month: Joi.alternatives()
			.try(Joi.number().min(1).max(12).required(),
				Joi.string().valid('*')),
		year: Joi.alternatives()
			.try(Joi.number().greater(2017).required(),
				Joi.string().valid('*'))
	}),
	end: Joi.object({
		hour: Joi.number().min(0).max(24).required(),
		minute: Joi.number().min(0).max(59).required(),
		second: Joi.number().min(0).max(59).required(),
		weekday: Joi.number().min(0).max(6).required(),
		day: Joi.alternatives()
			.try(Joi.number().min(1).max(31).required(),
				Joi.string().valid('*')),
		month: Joi.alternatives()
			.try(Joi.number().min(1).max(12).required(),
				Joi.string().valid('*')),
		year: Joi.alternatives()
			.try(Joi.number().greater(2017).required(),
				Joi.string().valid('*'))
	})
});

const OPTIONS_SCHEMA = Joi.object({

	id: Joi.number().required(),
	owner: Joi.string().required(),
	code: Joi.number().min(99999).required(),
	slots: Joi.array().items(SLOT_OBJECT_SCHEMA.required())

}).required();



module.exports = function checkCode(codeDef) {

	if(!codeDef) {
		throw new Error('Missing codeDef');
	}


	let { value, error } = Joi.validate(codeDef, OPTIONS_SCHEMA);

	if(error) {
		throw error;
	}

	return true;
}

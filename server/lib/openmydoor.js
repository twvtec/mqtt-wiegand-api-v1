'use strict';

const mqtt = require('mqtt');



module.exports = function openMyDoor(req, broker) {

	console.log('openMyDoor');


	let client = mqtt.connect(
		broker.host, {
			port: broker.port,
			username: broker.username,
			password: broker.password
		});

	client.on('connect', function() {
		console.log('connected to broker');
		client.subscribe('home/doors/' + req.auth.credentials.scope);
		client.publish('home/doors/' + req.auth.credentials.scope, 'open');
	});

	client.on('message', function(topic, message) {
		console.log(message.toString());
		client.end();
	})


	return {
		statusCode: 200,
		message: 'Login successful',
		scope: req.auth.credentials.scope
	}

}

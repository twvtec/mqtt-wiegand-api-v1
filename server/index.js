'use strict';

const Hapi = require('hapi');
const BasicAuthPlugin = require('hapi-auth-basic');
const fs = require('fs');
const path = require('path');

const checkCode = require('./lib/code-checker.js');
const openMyDoor = require('./lib/openmydoor.js');


// https://github.com/hapijs/joi/blob/v13.0.2/API.md
const Joi = require('joi');
const Boom = require('boom');



module.exports = async function(config) {

	let dbPath = path.resolve(config.get('dbPath'));
	let db = JSON.parse(fs.readFileSync(dbPath));

	console.log(db);

	let server = new Hapi.Server({
		port: config.get('server.port'),
		host: config.get('server.host')
	});

	await server.register(BasicAuthPlugin);

	server.auth.strategy('simple', 'basic', {
		validate: (req, deviceName, deviceSecret, h) => {


			let device = db.devices.find(d => d.name === deviceName);

			if (!device) {
				throw Boom.unauthorized('Unknown device');
			}

			if (device.secret !== deviceSecret) {
				throw Boom.unauthorized('Invalid secret');
			}

			return {
				credentials: Object.assign({}, device),
				isValid: true
			};

		}
	});

	server.route({
		path: '/',
		method: 'GET',
		async handler(req) {
			return Promise.resolve(123);
		}
	})

	server.route({
		path: '/openmydoor',
		method: 'POST',
		config: {
			auth: {
				scope: ['frontDoor'],
				strategy: 'simple'
			},
			validate: {
				payload: {
					code: Joi.number().required()
				}
			}
		},
		handler(req) {

			let code = req.payload.code;

			let dbPath = path.resolve(config.get('dbPath'));
			let db = JSON.parse(fs.readFileSync(dbPath));

			let codeDef = db.codes.find(c => c.code === code);

			if (codeDef) {

			}

			try {
				checkCode(codeDef);
				return openMyDoor(req, db.mqttBroker)
			} catch (e) {
				console.log(e);
				return Boom.unauthorized('Invalid code');
			}

			//console.log('Login success');
			//req.auth.credentials.scope
		}
	})

	return server;
};

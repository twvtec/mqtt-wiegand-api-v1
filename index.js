'use strict';

const boot = require('./server');
const config = require('config');

(async () => {

	try {

		let server = await boot(config);

		await server.start();

		console.log(server.info);


	} catch(e) {
		console.log(e);
		process.exit(1);
	}


})();

'use strict';

// Code Api: https://github.com/hapijs/code/blob/HEAD/API.md

const Code = require('code');
const expect = Code.expect;

const config = require('config');
const boot = require('../server');



function createBasicHeader(username, password) {
	return 'Basic ' + (new Buffer(username + ':' + password, 'utf8')).toString('base64');
};


describe('Server', function () {
	this.timeout(0);

	let server;
	beforeEach(async () => server = await boot(config));
	afterEach(() => server.stop());







	it('should return 123 on base route', async function () {

		let { result } = await server.inject({
			method: 'GET',
			url: '/',
			credentials: {
				app: 'esp8266-wiegand-hub'
			}
		});

		expect(result)
			.to.equal(123);

	});



	it('shoult return error on invalid code', async function () {

		let { result } = await server.inject({
			method: 'POST',
			url: '/openmydoor',
			credentials: {
				app: 'esp8266-wiegand-hub',
				scope: ['frontDoor']
			},
			payload: {
				code: '123456'
			}
		});

		expect(result.statusCode)
			.to.equal(401);


	});

	it('should return error on invalid payload', async function () {

		let { result } = await server.inject({
			method: 'POST',
			url: '/openmydoor',
			credentials: {
				app: 'esp8266-wiegand-hub',
				scope: ['frontDoor']
			},
			payload: {
				qwe: '123456'
			}
		});

		expect(result.statusCode)
			.to.equal(400);


	});


	it('should use auth-basic to open door', async function () {

		let { result } = await server.inject({
			method: 'POST',
			url: '/openmydoor',
			headers: {
				authorization: createBasicHeader('test', 'qwe123')
			},
			payload: {
				code: '123456'
			}
		});

		expect(result.statusCode)
				.to.equal(401);

			expect(result.error)
				.to.equal('Unauthorized');

			expect(result.message)
				.to.equal('Invalid code');
	});



	describe('Auth', function () {



		it('should respond with error on invalid device', async function () {

			let { result } = await server.inject({
				method: 'POST',
				url: '/openmydoor',
				headers: {
					authorization: createBasicHeader('unknown', '6543323')
				},
				payload: {
					code: '123456'
				}
			});

			expect(result.statusCode)
				.to.equal(401);

			expect(result.error)
				.to.equal('Unauthorized');

			expect(result.message)
				.to.equal('Unknown device');


		});

		it('should respond with error on invalid secret', async function () {

			let { result } = await server.inject({
				method: 'POST',
				url: '/openmydoor',
				headers: {
					authorization: createBasicHeader('test', '6543323')
				},
				payload: {
					code: '123456'
				}
			});

			expect(result.statusCode)
				.to.equal(401);

			expect(result.error)
				.to.equal('Unauthorized');

			expect(result.message)
				.to.equal('Invalid secret');


		});


	});

});

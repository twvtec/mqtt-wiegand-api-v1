'use strict';

const Code = require('code');
const expect = Code.expect;

const checkCode = require('../server/lib/code-checker');



describe('checkCode', function () {


	it('should throw error on missing codeDef', () => {

		expect(() => checkCode(), 'empty')
			.to.throw(Error, 'Missing codeDef');

		expect(() => checkCode(null), 'null')
			.to.throw(Error, 'Missing codeDef');

		expect(() => checkCode(undefined), 'undefined')
			.to.throw(Error, 'Missing codeDef');

	});

	it('should throw error on invalid codeDef', function () {

		expect(() => checkCode({}))
			.to.throw(Error);

	});


	it('should be ok without slots', function () {

		let res = checkCode({
			id: 123,
			owner: 'John Doe',
			code: 123333
		});

		expect(res)
			.to.be.true();

	});


	it('should throw error because slots is boolean', function () {

		expect(() => checkCode({
			id: 123,
			owner: 'John Doe',
			code: 123333,
			slots: true
		}))
			.to.throw(Error);
	});

	it('should be ok with slots array', function () {

		expect(() => checkCode({
			id: 123,
			owner: 'John Doe',
			code: 123333,
			slots: ["asd","qwe"]
		}))
			.to.throw(Error);
	});

	it('general check', function () {

		let res = checkCode({
			id: 2,
			owner: "Putzfrau",
			code: 123456,
			slots: [
				{
					start: {
						hour: 11,
						minute: 0,
						second: 0,
						weekday: 4,
						day: "*",
						month: "*",
						year: "*"
					},
					end: {
						hour: 12,
						minute: 15,
						second: 0,
						weekday: 4,
						day: "*",
						month: "*",
						year: "*"
					}
				}
			]
		});

		expect(res)
			.to.be.true();
	});

});
